import numpy as np
import scipy.stats.distributions as spdists
import time

import sys
from time import sleep

from kdelikelihood import LikelihoodComputer, ParallelLikelihoodComputer, get_bandwidth_by_silverman
from kdelikelihood.kdelikelihood import CPU_COUNT

class catchtime:
    def __init__(self, verbose=False) -> None:
        self.verbose = verbose
    
    def __enter__(self):
        self.time = time.perf_counter()
        return self

    def __exit__(self, type, value, traceback):
        self.time = time.perf_counter() - self.time
        self.readout = f"Time: {self.time:.3f} seconds"
        if self.verbose:
            print(self)

    def __str__(self):
        return self.readout

def get_true_and_estimated_log_likelihood(
    distributions, generatedSampleSize, fieldSampleSize=100, seed=0, **kwargs
):
    if not hasattr(distributions, "__iter__"):
        distributions = [distributions]
    else:
        distributions = list(distributions)

    fieldData = np.hstack([dist.rvs(size=fieldSampleSize, random_state=seed)[:, None] for dist in distributions])

    generatedData = np.hstack(
        [dist.rvs(size=generatedSampleSize, random_state=seed+1)[:, None] for dist in distributions]
    )

    domains = []
    for dist in distributions:
        if dist.support() == (0, np.inf):
            if dist.rvs(1).dtype.kind == "i":
                domains.append(2)
            else:
                domains.append(1)
        elif dist.support() == (-np.inf, np.inf):
            domains.append(0)
        else:
            raise ValueError("The domain of the distribution is {}, which is not supported.".format(dist.support()))

    bandwidths = get_bandwidth_by_silverman(fieldData, sampleSize=generatedSampleSize, dataGroups=kwargs.get("dataGroups", 1))

    numWorkers = 8
    with ParallelLikelihoodComputer(fieldData, bandwidths, domains, separateProcesses=False, numWorkers=numWorkers, **kwargs) as llcomp:
        estimatedLogL = llcomp.compute_log_likelihood(generatedData)

    logL = []
    for dist, data in zip(distributions, fieldData.T):
        if dist.rvs(1).dtype.kind == "i":
            logL.append(dist.logpmf(data).sum())
        else:
            logL.append(dist.logpdf(data).sum())

    trueLikelihood = np.sum(logL)
    
    return trueLikelihood, estimatedLogL


def test_distributions(**kwargs):
    dists = [
        [spdists.norm(3, 5), spdists.poisson(2)],
        [spdists.gamma(2), spdists.nbinom(5, 0.7)],
    ]

    for i, dist in enumerate(dists):
        print("Distribution", i)
        print("---------")
        for dataGroups in 1, 2:
            for n in [1000, 100000]:
                for correctBias in False, True, :
                    true, est = get_true_and_estimated_log_likelihood(
                        dist, n, dataGroups=dataGroups, correctBias=correctBias, atols=1e-5, **kwargs
                    )

                    print(
                        "n = {}, {} data groups, correct bias = {}".format(
                            n, dataGroups, correctBias
                        )
                    )
                    print("True       = {:7.2f}".format(true))
                    print("Estimate   = {:7.2f}".format(est))
                    print("Difference = {:7.2f}".format(true - est))
                    print("====================")
        print()
        print()

def test_sampling1D(x, mean, stdDev, h, n, N):
    from scipy.stats import distributions

    realDistribution = distributions.norm(mean, stdDev)
    realLogValue = realDistribution.logpdf(x)

    estimates = []
    logEstimates = []
    logEstimates2 = []
    correctedLogEstimates = []

    computer = LikelihoodComputer(np.array([[x]]), [1], [h], [0], atol=1e-12, correctBias=False)
    noBiasComputer = LikelihoodComputer(np.array([[x]]), [1], [h], [0], atol=1e-12)

    for i in range(N):
        print(i)
        sample = realDistribution.rvs(size=n)[:, None]
        estimates.append(distributions.norm(x, h).pdf(sample).mean())
        logEstimates.append(
            np.log(np.exp(-((sample - x) ** 2 / (2 * h * h))).sum())
            - np.log(n)
            - (np.log(2 * np.pi) / 2 + np.log(h))
        )
        logEstimates2.append(computer.compute_log_likelihood(sample))
        correctedLogEstimates.append(noBiasComputer.compute_log_likelihood(sample))

    print(np.var(estimates) / (1 / (2 * np.sqrt(np.pi) * n * h) * realDistribution.pdf(x)))
    print(
        "val={:6.2f}, mean={:6.2f}, meanCorrected={:6.2f}".format(
            realLogValue, np.mean(logEstimates), np.mean(correctedLogEstimates)
        )
    )
    print(
        "plain : bias={:6.2f}, var={:6.2f}".format(
            np.mean(logEstimates) - realLogValue, np.var(logEstimates)
        )
    )
    print(
        "plain2: bias={:6.2f}, var={:6.2f}".format(
            np.mean(logEstimates2) - realLogValue, np.var(logEstimates2)
        )
    )
    print(
        "corr. : bias={:6.2f}, var={:6.2f}".format(
            np.mean(correctedLogEstimates) - realLogValue, np.var(correctedLogEstimates)
        )
    )


def test_samplingND(N=500, n=50000, x=20, params=[0, 4, 2], h=0.005):
    from scipy.stats import distributions

    np.random.seed(1)
    
    realDistributions = [
        distributions.norm(params[0], 1),
        distributions.gamma(params[1]),
        distributions.poisson(params[2]),
    ]
    
    def get_sample(size):
        return np.hstack([d.rvs(size=size)[:, None] for d in realDistributions])
    
    if np.isscalar(x):
        x = get_sample(int(x))
    
    realLogValue = sum(sum(
        [d.logpdf(xxx) for d, xxx in zip(realDistributions[:2], xx)]
    ) + realDistributions[2].logpmf(xx[2]) for xx in x)


    estimates = []
    logEstimates = []
    logEstimates2 = []
    correctedLogEstimates = []
    correctedLogEstimates2 = []
    correctedLogEstimates3 = []

    weights = np.ones(len(x))
    bandwidths = [h, h, h]
    domains = [0, 1, 2]
    atol = 1e-100
    computer = LikelihoodComputer(x, weights, bandwidths, domains, atol=atol, correctBias=False)
    computer2 = ParallelLikelihoodComputer(x, bandwidths, domains, atols=atol, correctBias=False)
    noBiasComputer = LikelihoodComputer(x, weights, bandwidths, domains, atol=atol)
    noBiasComputer2 = ParallelLikelihoodComputer(x, bandwidths, domains, dataGroups=1, atols=atol)
    noBiasComputer3 = ParallelLikelihoodComputer(x, bandwidths, domains, dataGroups=3, atols=atol)
    
    
    
    for i in range(N):
        print(i)
        sample = get_sample(n)
        # estimates.append(distributions.norm(x, h).pdf(sample).mean())
        # logEstimates.append(np.log(np.exp(-((sample-x)**2 / (2*h*h))).sum())
        #                     - np.log(n) - (np.log(2*np.pi)/2 + np.log(h)))
        correctedLogEstimates.append(noBiasComputer.compute_log_likelihood(sample))
        logEstimates.append(computer.compute_log_likelihood(sample))
        logEstimates2.append(computer2.compute_log_likelihood(sample))
        correctedLogEstimates2.append(noBiasComputer2.compute_log_likelihood(sample))
        correctedLogEstimates3.append(noBiasComputer3.compute_log_likelihood(sample))

    print(
        "val={:6.2f}, mean={:6.2f}, meanCorrected={:6.2f}".format(
            realLogValue, np.mean(logEstimates), np.mean(correctedLogEstimates)
        )
    )
    print(
        "plain : bias={:6.2f}, var={:6.2f}".format(
            np.mean(logEstimates) - realLogValue, np.var(logEstimates)
        )
    )
    print(
        "plain2 : bias={:6.2f}, var={:6.2f}".format(
            np.mean(logEstimates2) - realLogValue, np.var(logEstimates2)
        )
    )
    print(
        "corr. : bias={:6.2f}, var={:6.2f}".format(
            np.mean(correctedLogEstimates) - realLogValue, np.var(correctedLogEstimates)
        )
    )
    print(
        "corr2.: bias={:6.2f}, var={:6.2f}".format(
            np.mean(correctedLogEstimates2) - realLogValue,
            np.var(correctedLogEstimates2),
        )
    )
    print(
        "corr3.: bias={:6.2f}, var={:6.2f}".format(
            np.mean(correctedLogEstimates3) - realLogValue,
            np.var(correctedLogEstimates3),
        )
    )
    import sys

    sys.exit()


def test_samplingND2(N=500, n=50000, x=[[1, 1, 1]], params=[0, 0, 0], h=0.005):
    from scipy.stats import distributions, multivariate_normal

    realDistribution = multivariate_normal(params, [[1, 0.2, 0.1], [0.1, 1, 0.1], [0.1, 0.2, 2]])
    realLogValue = realDistribution.logpdf(x[0])

    logEstimates = []
    logEstimates2 = []
    correctedLogEstimates = []
    correctedLogEstimates2 = []
    correctedLogEstimates3 = []

    weights = [1, 1, 1]
    bandwidths = [h, h, h]
    
    domains = [0, 1, 2]
    computer = LikelihoodComputer(x, weights, bandwidths, domains, atol=1e-10, correctBias=False)
    computer2 = ParallelLikelihoodComputer(x, bandwidths, domains, atol=1e-10, correctBias=False)
    noBiasComputer = LikelihoodComputer(x, weights, bandwidths, domains, atol=1e-10)
    noBiasComputer2 = ParallelLikelihoodComputer(x, bandwidths, domains, dataGroups=1, atols=1e-10)
    noBiasComputer3 = ParallelLikelihoodComputer(x, bandwidths, domains, dataGroups=3, atols=1e-10)

    for i in range(N):
        print(i)
        sample = realDistribution.rvs(size=n)
        logEstimates.append(computer.compute_log_likelihood(sample))
        logEstimates2.append(computer2.compute_log_likelihood(sample))
        correctedLogEstimates.append(noBiasComputer.compute_log_likelihood(sample))
        correctedLogEstimates2.append(noBiasComputer2.compute_log_likelihood(sample))
        correctedLogEstimates3.append(noBiasComputer3.compute_log_likelihood(sample))

    print(
        "val={:6.2f}, mean={:6.2f}, meanCorrected={:6.2f}".format(
            realLogValue, np.mean(logEstimates), np.mean(correctedLogEstimates)
        )
    )
    print(
        "plain : bias={:6.2f}, var={:6.2f}".format(
            np.mean(logEstimates) - realLogValue, np.var(logEstimates)
        )
    )
    print(
        "plain2 : bias={:6.2f}, var={:6.2f}".format(
            np.mean(logEstimates2) - realLogValue, np.var(logEstimates2)
        )
    )
    print(
        "corr. : bias={:6.2f}, var={:6.2f}".format(
            np.mean(correctedLogEstimates) - realLogValue, np.var(correctedLogEstimates)
        )
    )
    print(
        "corr2.: bias={:6.2f}, var={:6.2f}".format(
            np.mean(correctedLogEstimates2) - realLogValue,
            np.var(correctedLogEstimates2),
        )
    )
    print(
        "corr3.: bias={:6.2f}, var={:6.2f}".format(
            np.mean(correctedLogEstimates3) - realLogValue,
            np.var(correctedLogEstimates3),
        )
    )
    import sys

    sys.exit()


def profile_routine(N=1000, n=20000, params=[0, 0, 0, 1, 5], waitTime=30, dtype=np.float64):
    from scipy.stats import multivariate_normal, distributions

    params = np.array(params)
    realDistributions = [multivariate_normal(params[:3], [[1, 0.2, 0.1], [0.2, 1, 0.1], [0.1, 0.1, 2]]),
                         distributions.gamma(params[3]),
                         distributions.poisson(params[4])]
    
    params += 1
    testDistributions = [multivariate_normal(params[:3], [[1, 0.2, 0.1], [0.2, 1, 0.1], [0.1, 0.1, 2]]),
                         distributions.gamma(params[3]),
                         distributions.poisson(params[4])]
    
    def get_data(nn, stateOffset, dists=realDistributions):
        data = []
        for i, realDistribution in enumerate(dists):
            d = realDistribution.rvs(size=nn, random_state=stateOffset+i)
            if d.ndim == 1:
                d = d[:,None]
            data.append(d)
            
        
        result = np.hstack(data, dtype=dtype)
        result[0] = -100
        return result

    domains = [0, 0, 0, 1, 2]
    
    dataGroups = 1
    
    x = get_data(N, 0)
    sample = get_data(n, 100, testDistributions)
    bandwidths = get_bandwidth_by_silverman(x, sampleSize=n, dataGroups=dataGroups)
    
    for i in range(waitTime, 0, -1):
        print(i, "seconds left.")
        sleep(1)
    
    print("Start.")
    
    with ParallelLikelihoodComputer(x, bandwidths, domains, dataGroups=dataGroups, atols=1e-5, separateProcesses=False, numWorkers=8, dtype=np.float64) as computer:
        with catchtime() as timer:
            for i in range(10):
                print(i)
                nLL = computer.compute_log_likelihood(sample)
    
    print(timer)
    print("Done.", nLL)


if __name__ == "__main__":
    profile_routine(waitTime=0)
    sys.exit()
    test_distributions(seed=2)
    test_samplingND(N=100, n=1000, x=20, params=[0, 4, 2], h=0.005)

    data = np.array([[0.6, 1.4, 2], [0.2, 1.5, 5]])

    obs = np.array(
        [
            [2, 1.9, 2],
            [np.nan, 1, 0],
            [21, 3, 4],
        ]
    )
    obs2 = np.array(
        [
            [2, 1.9, 2],
            [np.nan, 1, 0],
            [21, 3, 4],
            [21, 3, 4],
        ]
    )
    domains = [1, 1, 2]
    weights = np.ones(obs.shape[0])
    weights[-1] = 2

    consideredColumns = np.arange(data.shape[1])
    atol = 1e-10
    print("minimalLookupDistance", -np.log(atol))
    kernelStds = np.arange(1, 4)
    res2 = LikelihoodComputer(
        obs, weights, kernelStds, domains, consideredColumns, atol
    ).compute_log_likelihood(data)

    with ParallelLikelihoodComputer(obs2, kernelStds, domains, 1, atol, numWorkers=1) as cmp:
        res3 = cmp.compute_log_likelihood(data)

    print("res2", res2)
    print("res3", res3)
    print("Done")
